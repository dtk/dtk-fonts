// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkFonts/dtkFontAwesome>

int main(int argc, char **argv)
{
    QApplication::setAttribute(Qt::AA_UseOpenGLES, true);
    //QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    //QApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);

    QApplication application(argc, argv);
    application.setApplicationName("dtkFontsCollection");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

#if defined(Q_OS_IOS)
    Q_INIT_RESOURCE(dtkFontAwesome);
#endif

    dtkFontAwesome::instance()->initFontAwesome();
    dtkFontAwesome::instance()->setDefaultOption("color", QColor("#ff0000"));

    QGridLayout *layout = new QGridLayout;

    int total = dtkFontAwesome::instance()->namedCodePoints().keys().count();
    QList<QString> ordered;
    ordered << dtkFontAwesome::instance()->namedCodePoints().keys();
    ordered.sort();
    int index = 0;
    int maxline = 16;
    for(int i = 0; i < (total/maxline+1); i++) {
        for (int j = 0; j < maxline; j++) {
            if ( index < total ) {
                QLabel *item = new QLabel;
                QString key = ordered[index];
                item->setPixmap(dtkFontAwesome::instance()->icon(key).pixmap(32, 32));
                item->setToolTip(key);
                layout->addWidget(item, i, j);
            }
            index++;
        }
    }

    QFrame *contents = new QFrame;
    contents->setLayout(layout);

    QScrollArea *area = new QScrollArea;
    area->setWindowTitle("dtkFontsCollection");
    area->setWidget(contents);
    area->setWidgetResizable(true);
    area->show();

    return application.exec();
}

//
// main.cpp ends here
