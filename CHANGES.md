# ChangeLog
## version 3.2.0 - 2025-01-09
- switch to qt 6.8
## version 3.1.0 - 2023-11-09
- update cmakelists
- switch to c++20
## version 3.0.0 - 2023-09-22
- update to Qt6
## version 2.1.2 - 2022-05-23
- switch to c++17
## version 2.1.1 - 2019-05-16
- beautifying
## version 2.1.0 - 2019-05-16
- cmake refactoring
## version 2.0.2 - 2018-09-10
- change install of lib for windows
## version 2.0.1 - 2018-07-02
- add version in .so
## version 2.0.0 - 2018-05-18
- initial release
