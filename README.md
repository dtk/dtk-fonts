# Regular build

```shell
$ mkdir build; cd build
$ cmake ..
$ make
```

# iOS build - WIP

```shell
$ set -x PATH /Users/jwintz/Development/qt/5.12.3/ios/bin $PATH
$ mkdir build; cd build
$ cmake .. -DCMAKE_TOOLCHAIN_FILE=../../ios-cmake/ios.toolchain.cmake -DPLATFORM=SIMULATOR64
$ make

$ xcrun simctl install booted ./bin/dtkFontsCollection.app
$ xcrun simctl launch --console-pty booted fr.inria.dtkFontsCollection
```
